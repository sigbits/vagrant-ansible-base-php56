**Ansible provisioned base box for php5.6 dev**

Gives you an Ubuntu/trusty64 Vagrant box with 1GB RAM and:

- composer
- apache
- php56 with: 
    - php5-cli
    - php5-intl
    - php5-mcrypt
    - php5-mysql
    - php5-curl
- phpunit
- xdebug
- vim
- git

--------------
**Configuration**

Configuration applies to Vagrant as well as Ansible. Some parameters, like hostnames, are shared between both systems.
 Shared parameters should be configured in `ansible/vars/shared.yml` which is kept out of version control (copy the .dist file to get started). 
 Configure at least vagrant_box.hostname and general.servername. 
 
**Project files**
Project files are in a `project` subdirectory. This directory is git-ignored to enable separation of the Vagrant/Ansible repo and project repositories.
The `project` subdirectory is accessible via NFS in the box using the `/vagrant` mount point.

**First time use**
If you are on a Mac, install VirtualBox and clone this repo. Then configure the parameters in the `shared.yml` file and `vagrant up` will bring up 
the box and provision it using Ansible. If you are on Windows, you have no native support for NFS shares. There is a Vagrant plugin for that but it is 
not in this repo (yet).

**Forking and modifying**
If you need per project configurations, make sure you add them in `shared.yml` (and `shared.yml.dist` if you push to VCS). For an example of how
to use those params in Vagrant check the way the `hostname` parameter is extracted and initialized in the `Vagrantfile`.
